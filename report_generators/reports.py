import pandas as pd
from datetime import datetime
from utils.constants import Constants

class Reports:
    def __init__(self):
        pass

class TaxiReports(Reports):
    def __init__(self):
        pass

    @staticmethod
    def get_df_report_top_tipping_zones(df_data: pd.DataFrame, df_lookup: pd.DataFrame, top_n: int = 5) -> pd.DataFrame:
        # Report Query
        df_agg = df_data.groupby('DOLocationID').agg({'tip_amount' : 'sum'}).rename(columns={'tip_amount':'total_tip_amount'}).reset_index() \
                .sort_values(by='total_tip_amount', ascending=False)
        
        df_top_tipping_zones = pd.merge(df_agg, df_lookup,  how='left', left_on=['DOLocationID'], right_on = ['LocationID'], validate = 'm:1')[['Borough','Zone','total_tip_amount']].head(top_n)
        
        return df_top_tipping_zones
    @staticmethod
    def get_df_report_longest_trip_per_day(df_data: pd.DataFrame, df_lookup: pd.DataFrame, start_date: str = '2018-01-01', end_date: str  = '2018-01-07', top_n: int = 5) -> pd.DataFrame:
        # Transform dataframe
        df_select = df_data.loc[:,['tpep_dropoff_datetime','trip_distance','DOLocationID']]
        df_select['tpep_dropoff_datetime'] = pd.to_datetime(df_select['tpep_dropoff_datetime'], format='%Y-%m-%d %H:%M:%S')
        df_select['dropoff_date'] = df_select['tpep_dropoff_datetime'].dt.date
        
        # Clean dataframe for report
        df_select = df_select[~df_select['DOLocationID'].isin(Constants.UNKNOWN_LOCATIONS)]
        df_select = df_select[(df_select['tpep_dropoff_datetime'] >= start_date) & (df_select['tpep_dropoff_datetime'] <= end_date)]

        # Report Query
        df_agg = df_select.sort_values(['dropoff_date','trip_distance'],ascending = [True, False]).groupby('dropoff_date').head(top_n).reset_index()
        df_longest_trip_per_day = pd.merge(df_agg, df_lookup,  how='left', left_on=['DOLocationID'], right_on = ['LocationID'], validate = 'm:1')[['dropoff_date', 'tpep_dropoff_datetime', 'Borough', 'Zone','trip_distance']]
        
        return df_longest_trip_per_day