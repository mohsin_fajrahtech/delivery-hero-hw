class Constants:
    def __init__(self):
        pass

    LOOKUP_FILE_URL = 'https://s3.amazonaws.com/nyc-tlc/misc/taxi+_zone_lookup.csv'
    DATA_FOLDER_PATH = 'data/'
    UNKNOWN_LOCATIONS = [264,265]