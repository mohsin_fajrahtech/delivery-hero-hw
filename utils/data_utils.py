import os
import pandas as pd
from urllib.request import urlretrieve
from utils.logger import init_logger
from utils.constants import Constants

logger = init_logger("data_utils.py")

def import_df_from_csv(url: str) -> pd.DataFrame:
    data_folder_path = Constants.DATA_FOLDER_PATH
    
    # If directory does not exist, create
    if not os.path.exists(data_folder_path):
        os.makedirs(data_folder_path)
    
    # If file exists, skip the download
    file_name = f"{data_folder_path}{url.rsplit('/', 1)[-1]}"
    
    if not os.path.exists(file_name):
        logger.info(f"Downloading file: {url} - in progress ...")
        urlretrieve(url, file_name)
        logger.info(f"Downloading file: {url} - done!")
        
    return pd.read_csv(file_name)

def export_df_to_csv(df: pd.DataFrame, path: str, file_name: str):
    # If directory does not exist, create
    if not os.path.exists(path):
        os.makedirs(path)

    df.to_csv(f'{path}/{file_name}.csv', index = False)