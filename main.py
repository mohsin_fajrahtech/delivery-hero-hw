# Sample command: python3 main.py -i "https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2018-01.csv" -o "./output_report"
import sys, getopt

import pandas as pd
from utils.constants import Constants
from utils.data_utils import import_df_from_csv, export_df_to_csv
from utils.logger import init_logger
from report_generators.reports import TaxiReports

logger = init_logger("main.py")
def main(argv):
  input_url = ''
  output_path = ''
  try:
    opts, args = getopt.getopt(argv,"hi:o:",["iurl=","opath="])
  except getopt.GetoptError:
    logger.error("main.py -i <inputUrl> -o <outputPath>")
    raise Exception(getopt.GetoptError)
  
  for opt, arg in opts:
    if opt == '-h':
      logger.info('main.py -i <inputUrl> -o <outputPath>')
      sys.exit()
    elif opt in ("-i", "--iurl"):
      input_url = arg
    elif opt in ("-o", "--opath"):
      output_path = arg

  logger.info(f'Input Url: {input_url}')
  logger.info(f'Output Path: {output_path}')

  try:
    data_file_url = input_url # 'https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2018-01.csv'
    lookup_file_url = Constants.LOOKUP_FILE_URL

    df_data = import_df_from_csv(data_file_url)
    df_lookup = import_df_from_csv(lookup_file_url)

    df_report_top_tipping_zones = TaxiReports.get_df_report_top_tipping_zones(df_data, df_lookup)
    df_report_longest_trip_per_day = TaxiReports.get_df_report_longest_trip_per_day(df_data, df_lookup)

    export_df_to_csv(df_report_top_tipping_zones, output_path,'top_tipping_zones')
    export_df_to_csv(df_report_longest_trip_per_day, output_path,'longest_trip_per_day')

  except Exception as e:
    logger.error(e)
    raise Exception(e)

if __name__ == "__main__":
  main(sys.argv[1:])