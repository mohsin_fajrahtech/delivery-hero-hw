#!/bin/sh

echo "Virtual environment creation - in progress!"
python3 -m venv env
echo "Virtual environment creation - done!"

source env/bin/activate
echo "Virtual environment - activated!"

echo "Resolving Dependency - in progress!"
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
echo "Resolving Dependency - done!"

echo "main.py - in progress!"
python3 main.py -i "https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2018-01.csv" -o "./output_report"
echo "main.py - done!"

deactivate